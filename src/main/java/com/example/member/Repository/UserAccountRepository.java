package com.example.member.Repository;

import java.util.Optional;

import com.example.member.Entity.UserAccount;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
    Optional<UserAccount> findByPhoneNumber(String phoneNumber);
    Optional<UserAccount> findByEmail(String email);
    Boolean existsByPhoneNumber(String phoneNumber);
    Boolean existsByEmail(String email);
    Optional<UserAccount> findByEmailOrPhoneNumber(String email, String phoneNumber);
}