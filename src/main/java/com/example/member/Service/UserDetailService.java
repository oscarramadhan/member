package com.example.member.Service;

import com.example.member.Entity.UserAccount;
import com.example.member.Model.UserPrinciple;
import com.example.member.Repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserDetailService implements UserDetailsService {
 
    @Autowired
    UserAccountRepository userAccountRepository;
 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // String[] emailAndPhoneNumber = username.split("|", 2);
        UserAccount user = userAccountRepository.findByEmailOrPhoneNumber(username, username)
                        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with -> username : " + username));
 
        return UserPrinciple.build(user);
    }

    public UserDetails loadUserByEmailAndPhoneNumber(String username) throws UsernameNotFoundException {
        String[] emailAndPhoneNumber = username.split("\\|", 2);
        UserAccount user = userAccountRepository.findByEmailOrPhoneNumber(emailAndPhoneNumber[0], emailAndPhoneNumber[1])
                        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with -> username : " + username));
 
        return UserPrinciple.build(user);
    }
}
