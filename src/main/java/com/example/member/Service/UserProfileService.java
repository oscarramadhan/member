package com.example.member.Service;

import javax.transaction.Transactional;

import com.example.member.Entity.UserAccount;
import com.example.member.Entity.UserProfile;
import com.example.member.Repository.UserProfileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class UserProfileService implements IUserProfileService {
    @Autowired
    UserProfileRepository userProfileRepository;

    @Override
    public void add(UserProfile userProfile, UserAccount userAccount) {
        UserProfile newUserProfile = new UserProfile(userProfile.getFirstName(), userProfile.getLastName(), userProfile.getGender(), userProfile.getDateOfBirth(), userAccount);
        userProfileRepository.save(newUserProfile);
    }

    @Override
    public UserProfile getUserProfile(Long id) {
        return userProfileRepository.findById(id).get();
    }
}