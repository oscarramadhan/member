package com.example.member.Service;

import com.example.member.Entity.UserAccount;
import com.example.member.Entity.UserProfile;

public interface IUserProfileService {
    void add(UserProfile userProfile, UserAccount userAccount);
    UserProfile getUserProfile(Long id);
}
