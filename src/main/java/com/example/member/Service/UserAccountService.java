package com.example.member.Service;

import javax.transaction.Transactional;

import com.example.member.Entity.UserAccount;
import com.example.member.Model.SignUpForm;
import com.example.member.Repository.UserAccountRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class UserAccountService implements IUserAccountService {
    @Autowired
    UserAccountRepository userAccountRepository;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public UserAccount findUserAccount(String username) {
        String[] emailAndPhoneNumber = username.split("\\|", 2);
        UserAccount userAccount = userAccountRepository.findByEmailOrPhoneNumber(emailAndPhoneNumber[0], emailAndPhoneNumber[1])
            .orElseThrow(() -> new UsernameNotFoundException("User Not Found with -> username : " + username));

        return userAccount;
    }

    @Override
    public boolean existsByPhoneNumber(String phoneNumber) {
        return userAccountRepository.existsByPhoneNumber(phoneNumber);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userAccountRepository.existsByEmail(email);
    }

    @Override
    public void add(SignUpForm signUpRequest) {
        UserAccount user = new UserAccount(
            signUpRequest.getPhoneNumber(),
            signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword())
        );

        userAccountRepository.save(user);

    }
}