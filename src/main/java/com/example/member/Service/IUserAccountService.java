package com.example.member.Service;

import com.example.member.Entity.UserAccount;
import com.example.member.Model.SignUpForm;

public interface IUserAccountService {
    UserAccount findUserAccount(String username);
    boolean existsByPhoneNumber(String phoneNumber);
    boolean existsByEmail(String email);
    void add(SignUpForm signUpRequest);
}
