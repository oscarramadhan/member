package com.example.member.Entity;
 
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
@Entity
@Table(name = "user_profile")
public class UserProfile{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", length = 50, nullable = false, columnDefinition = "VARCHAR")
    private String firstName;
 
    @Column(name = "last_name", length = 50, nullable = false, columnDefinition = "VARCHAR")
    private String lastName;

    @Column(name = "gender", length = 1, nullable = false,  columnDefinition = "CHAR")
    private char gender;

    @Column(name = "date_of_birth", nullable = false,  columnDefinition = "DATE")
    private Date dateOfBirth;

    
    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_account_id", referencedColumnName = "id")
    private UserAccount userAccount;

    public UserProfile () {
    }

    public UserProfile(String firstName, String lastName, char gender, Date dateOfBirth, UserAccount userAccount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.userAccount = userAccount;
    }

    public Long getId() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public char getGender() {
        return this.gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return this.dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}