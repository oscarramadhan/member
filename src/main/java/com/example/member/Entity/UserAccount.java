package com.example.member.Entity;
 
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
 
@Entity
@Table(name = "user_account", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "phone_number"
        }),
        @UniqueConstraint(columnNames = {
            "email"
        })
})
public class UserAccount{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
 
    @NotBlank
    @Size(min=3, max = 50)
    @Column(name = "phone_number", length = 50, columnDefinition = "VARCHAR")
    private String phoneNumber;
 
    @NotBlank
    @Size(max = 50)
    @Email
    @Column(name = "email", length = 50, columnDefinition = "VARCHAR")
    private String email;

    @NotBlank
    @Size(min = 6, max = 100)
    @Column(name = "password", length = 100, columnDefinition = "VARCHAR")
    private String password;

    @OneToOne(mappedBy = "userAccount")
    private UserProfile userProfile;

    public UserAccount() {
    }

    public UserAccount(String phoneNumber, String email, String password) {
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
    }
 
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }
 
    public String getPhoneNumber() {
        return phoneNumber;
    }
 
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
}