package com.example.member.Controller;

import javax.validation.Valid;

import com.example.member.Entity.UserAccount;
import com.example.member.Entity.UserProfile;
import com.example.member.Security.JwtProvider;
import com.example.member.Service.IUserAccountService;
import com.example.member.Service.IUserProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserProfileController { 
    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    IUserProfileService iUserProfileService;

    @Autowired
    IUserAccountService IUserAccountService;

    @PutMapping("/profile")
    public ResponseEntity<?> putProfile(@RequestHeader HttpHeaders httpHeaders, @RequestBody @Valid UserProfile userProfile) {
        String token = httpHeaders.get("Authorization").get(0).replace("Bearer ", "");
        String username = jwtProvider.getUserNameFromJwtToken(token);
        UserAccount userAccount = IUserAccountService.findUserAccount(username);
        iUserProfileService.add(userProfile, userAccount);
        return new ResponseEntity<>("Profile Updated", HttpStatus.OK);
    }

    @GetMapping("/profile/{id}")
    public ResponseEntity<?> getProfile(@PathVariable Long id) {
        UserProfile userProfile = iUserProfileService.getUserProfile(id);
        return new ResponseEntity<>(userProfile, HttpStatus.OK);
    }
}