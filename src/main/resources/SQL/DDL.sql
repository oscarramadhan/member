create table `user_account` (
  `id` bigint(20) not null auto_increment,
  `email` varchar(50) not null unique,
  `phone_number` varchar(50) not null unique,
  `password` varchar(100) not null,
  primary key (`id`)
) ENGINE=InnoDB auto_increment=1 default CHARSET=latin1;

create table `user_profile` (
	`id` bigint(20) not null auto_increment,
	`first_name` varchar(50) not null,
	`last_name` varchar(50) not null,
	`gender` bool not null,
	`date_of_birth` date not null,
	`user_account_id` bigint(20) not null,
	primary key (`id`),
	foreign key (`user_account_id`) references `user_account`(`id`)
) ENGINE=InnoDB auto_increment=1 default CHARSET=latin1;